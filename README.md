# cloud-disk-spring-boot-starter

#### 介绍
云盘对接
-- 百度网盘

#### 软件架构
自定义starter


#### 使用说明

cloud:
    disk:
        # 目前只对接了百度
        type: baidu
        baidu:
            appId: 40738602
            appKey: c8s****LLqqH
            secretkey: Qx4O***2s4vHMEsBG
            signKey: 9k#6wy***9CvBBolbS
            # 若有自己的域名，可在此处设置百度的回调地址:具体请参考百度网盘对接文档
            redirectUri: oob

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
