package com.zengjianjun.cloud.disk.annotation;

import com.zengjianjun.cloud.disk.config.CloudDiskAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author zengjianjun
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({CloudDiskAutoConfiguration.class})
@Documented
@Inherited
public @interface EnableCloudDisk {
}
