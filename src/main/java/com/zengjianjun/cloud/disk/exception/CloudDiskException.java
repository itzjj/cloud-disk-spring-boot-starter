package com.zengjianjun.cloud.disk.exception;

import cn.hutool.core.text.CharPool;
import com.zengjianjun.cloud.disk.enums.CloudDiskTypeEnum;
import org.apache.commons.lang3.StringUtils;

/**
 * 云盘处理异常类
 *
 * @author zengjianjun
 */
public class CloudDiskException extends RuntimeException {

    public CloudDiskException(String message) {
        super(message);
    }

    public CloudDiskException(Throwable throwable) {
        super(throwable);
    }

    public CloudDiskException(String message, Throwable throwable) {
        super(message, throwable);
    }

    /**
     * 返回一个新的异常，统一构建，方便统一处理
     *
     * @param msg    消息
     * @param t      异常
     * @param params 参数
     * @return 自定义异常
     */
    public static CloudDiskException build(String msg, Throwable t, Object... params) {
        return new CloudDiskException(String.format(msg, params), t);
    }

    /**
     * 重载
     *
     * @param msg    消息
     * @param params 参数
     * @return 自定义异常
     */
    public static CloudDiskException build(CloudDiskTypeEnum cloudDiskTypeEnum, String msg, Object... params) {
        if (StringUtils.contains(msg, "{}")) {
            msg = StringUtils.replace(msg, "{}", "%s");
        }
        return new CloudDiskException(CharPool.BRACKET_START + cloudDiskTypeEnum.name() + CharPool.BRACKET_END + String.format(msg, params));
    }

    public static CloudDiskException build(String msg, Object... params) {
        if (StringUtils.contains(msg, "{}")) {
            msg = StringUtils.replace(msg, "{}", "%s");
        }
        return new CloudDiskException(String.format(msg, params));
    }

    /**
     * 重载
     *
     * @param t 异常
     * @return 自定义异常
     */
    public static CloudDiskException build(Throwable t) {
        return new CloudDiskException(t);
    }
}
