package com.zengjianjun.cloud.disk.utils;

import cn.hutool.core.text.CharPool;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author zengjianjun
 */
public class RemoteUrlUtil {

    /**
     * 获取curl下载文件命令
     *
     * @param remoteUrl 远程链接
     * @return curl命令
     */
    public static List<String> getCurlCommand(String remoteUrl, Map<String, String> headMap) {
        return getCurlCommand(remoteUrl, null, headMap);
    }

    /**
     * 获取curl下载文件命令
     *
     * @param remoteUrl    远程链接
     * @param descFilePath 目标地址
     * @return curl命令
     */
    public static List<String> getCurlCommand(String remoteUrl, String descFilePath, Map<String, String> headMap) {
        // 构建命令参数
        List<String> command = new ArrayList<>();
        command.add("curl");
        if (StringUtils.isNotEmpty(descFilePath)) {
            command.add("-o");
            command.add(descFilePath);
        }
        command.add("--location");
        command.add("--request");
        command.add("GET");
        command.add(remoteUrl);
        if (headMap != null && !headMap.isEmpty()) {
            headMap.forEach((key, value) -> {
                command.add("--header");
                command.add(key + CharPool.COLON + value);
            });
        }
        return command;
    }
}
