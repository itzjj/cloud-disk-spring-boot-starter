package com.zengjianjun.cloud.disk.enums;

/**
 * @author zengjianjun
 */
public interface IEnum<T, V> {

    T getCode();

    V getDesc();
}
