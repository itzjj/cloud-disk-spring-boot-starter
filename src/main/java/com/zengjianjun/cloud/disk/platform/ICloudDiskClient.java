package com.zengjianjun.cloud.disk.platform;

import com.zengjianjun.cloud.disk.enums.CloudDiskTypeEnum;
import com.zengjianjun.cloud.disk.result.AuthInfoResult;
import com.zengjianjun.cloud.disk.result.FileInfoResult;
import com.zengjianjun.cloud.disk.result.FileListResult;
import com.zengjianjun.cloud.disk.result.TokenInfoResult;

import java.util.List;

/**
 * 网盘操作相关基础接口
 *
 * @author zengjianjun
 */
public interface ICloudDiskClient {

    /**
     * 获取当前云盘类型
     *
     * @return 云盘类型
     */
    CloudDiskTypeEnum get();

    /**
     * 获取授权信息
     *
     * @return 授权信息
     */
    AuthInfoResult getAuthInfo();

    /**
     * 设置token
     */
    void setTokenInfo(TokenInfoResult tokenInfoResult);

    /**
     * 获取token
     *
     * @return token信息
     */
    TokenInfoResult getTokeInfo();

    /**
     * 刷新token
     *
     * @return 授权信息
     */
    TokenInfoResult refreshAccessToken();

    /**
     * 获取指定目录下的文件列表
     *
     * @param path 目录地址
     * @return 文件详情信息
     */
    List<FileListResult> getFileList(String path);

    /**
     * 获取文件详情信息
     *
     * @param fileId 文件Id
     * @return 文件详情信息
     */
    FileInfoResult getFileInfo(String fileId);

    /**
     * 批量获取文件详情信息
     *
     * @param fileIdList 文件Id列表
     * @return 文件详情信息
     */
    List<FileInfoResult> getFileInfoList(List<String> fileIdList);

    /**
     * 将文件下载到指定目录
     *
     * @param fileId 文件id
     * @param dir    目录
     * @return 本地文件完成路径
     */
    String download(String fileId, String dir);

    /**
     * 将文件下载到指定目录
     *
     * @param fileId 文件id
     * @param dir    目录
     * @param size   指定大小
     * @return 本地文件完成路径
     */
    String download(String fileId, String dir, Integer size);
}
