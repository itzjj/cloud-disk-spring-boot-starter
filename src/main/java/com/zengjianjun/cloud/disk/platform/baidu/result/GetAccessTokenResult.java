package com.zengjianjun.cloud.disk.platform.baidu.result;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author zengjianjun
 */
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class GetAccessTokenResult extends BaiduBaseResult {

    /**
     * 获取到的授权token，作为调用其他接口访问用户数据的凭证
     */
    @JSONField(name = "access_token")
    private String accessToken;

    /**
     * access_token的有效期，单位：秒
     */
    @JSONField(name = "expires_in")
    private Integer expiresIn;

    /**
     * 用于刷新access_token, 有效期为10年
     */
    @JSONField(name = "refresh_token")
    private String refreshToken;

    /**
     * access_token最终的访问权限，即用户的实际授权列表
     */
    @JSONField(name = "scope")
    private String scope;
}
