package com.zengjianjun.cloud.disk.platform.baidu.result;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * 文件详情信息
 *
 * @author zengjianjun
 */
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class GetFileInfoResult extends BaiduBaseResult {

    /**
     * 文件详情列表
     */
    private List<FileInfo> list;


    @Data
    @SuperBuilder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class FileInfo {
        /**
         * 文件类型，含义如下：1 视频， 2 音乐，3 图片，4 文档，5 应用，6 其他，7 种子
         */
        private Integer category;
        /**
         * 文件类型，含义如下：1 视频， 2 音乐，3 图片，4 文档，5 应用，6 其他，7 种子
         */
        @JSONField(name = "dlink")
        private String dLink;
        /**
         * 文件名
         */
        private String filename;
        /**
         * 是否是目录，为1表示目录，为0表示非目录
         */
        private Integer isdir;
        /**
         * 文件的服务器创建Unix时间戳，单位秒
         */
        @JSONField(name = "server_ctime")
        private Integer serverCtime;
        /**
         * 文件的服务器修改Unix时间戳，单位秒
         */
        @JSONField(name = "server_mtime")
        private Integer serverMtime;
        /**
         * 文件大小，单位字节
         */
        private Integer size;
        /**
         * 文件路径
         */
        private String path;

        @JSONField(name = "oper_id")
        private String operId;

        /**
         * 文件的本地创建Unix时间戳，单位秒
         */
        @JSONField(name = "local_ctime")
        private Integer localCtime;
        /**
         * 文件的本地修改Unix时间戳，单位秒
         */
        @JSONField(name = "server_mtime")
        private Integer localMtime;
        /**
         * 文件md5
         */
        private String md5;
    }
}
