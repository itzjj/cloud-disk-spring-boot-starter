package com.zengjianjun.cloud.disk.platform;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author zengjianjun
 */
public abstract class AbstractCloudDiskClient implements ICloudDiskClient {

    /**
     * 计算下载进度
     *
     * @param current 当前已下载大小
     * @param total   总大小
     * @return 下载百分比
     */
    protected String progress(int current, int total) {
        BigDecimal h = new BigDecimal("100");
        BigDecimal a = new BigDecimal(String.valueOf(current));
        BigDecimal b = new BigDecimal(String.valueOf(total));
        return h.multiply(a.divide(b, 3, RoundingMode.HALF_UP)).doubleValue() + "%";
    }
}
