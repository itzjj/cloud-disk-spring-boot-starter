package com.zengjianjun.cloud.disk.platform.baidu.enums;

import com.zengjianjun.cloud.disk.enums.IEnum;
import com.zengjianjun.cloud.disk.platform.baidu.result.BaiduBaseResult;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 微信api响应状态码
 *
 * @author zengjianjun
 */
public enum BaiduCodeEnum implements IEnum<Integer, String> {

    SUCCESS(0, "请求成功"),
    UNKNOWN_ERROR(1, "未知错误，如果频繁发生此错误，请联系developer_support@baidu.com"),
    SERVICE_TEMPORARILY_UNAVAILABLE(2, "服务暂时不可用"),
    UNSUPPORTED_OPENAPI_METHOD(3, "访问URL错误，该接口不能访问"),
    OPEN_API_REQUEST_LIMIT_REACHED(4, "该APP访问该接口的QPS达到上限"),
    UNAUTHORIZED_CLIENT_IP_ADDRESS(5, "访问的客户端IP不在白名单内"),
    NO_PERMISSION_TO_ACCESS_DATA(6, "该APP没有访问该接口的权限"),
    OPEN_API_DAILY_REQUEST_LIMIT_REACHED(17, "该APP访问该接口超过每天的访问限额"),
    OPEN_API_QPS_REQUEST_LIMIT_REACHED(18, "该APP访问该接口超过QPS限额"),
    OPEN_API_TOTAL_REQUEST_LIMIT_REACHED(19, "该APP访问该接口超过总量限额"),
    INVALID_PARAMETER(100, "没有获取到token参数"),
    ACCESS_TOKEN_INVALID_OR_NO_LONGER_VALID(110, "token不合法"),
    ACCESS_TOKEN_EXPIRED(111, "token已过期"),
    NO_PERMISSION_TO_ACCESS_USER_MOBILE(213, "没有权限获取用户手机号"),

    AUTH_ERROR(-6, "身份验证失败"),

    FILE_NO_PERMISSION(-7, "文件或目录无权访问"),
    FILE_NO_EXIST(-9, "文件或目录不存在"),

    IMAGE_INFO_FIND_ERROR(42211, "图片详细信息查询失败"),
    SHARE_FILE_FIND_ERROR(42212, "共享目录文件上传者信息查询失败，可重试"),
    SHARE_FILE_NO_PERMISSION(42213, "共享目录鉴权失败"),
    FILE_FIND_ERROR(42214, "文件基础信息查询失败"),
    ;

    BaiduCodeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private final Integer code;
    private final String desc;

    public static boolean isSuccess(BaiduBaseResult baiduBaseResult) {
        return baiduBaseResult != null && SUCCESS.getCode().equals(baiduBaseResult.getErrno());
    }

    public static boolean isTokenError(BaiduBaseResult baiduBaseResult) {
        List<Integer> list = new ArrayList<>();
        list.add(INVALID_PARAMETER.getCode());
        list.add(ACCESS_TOKEN_INVALID_OR_NO_LONGER_VALID.getCode());
        list.add(ACCESS_TOKEN_EXPIRED.getCode());
        list.add(AUTH_ERROR.getCode());
        return baiduBaseResult != null && (list.contains(baiduBaseResult.getErrno()));
    }

    public static String getErrorMsg(BaiduBaseResult baiduBaseResult) {
        if (StringUtils.isNotEmpty(baiduBaseResult.getErrmsg())) {
            return baiduBaseResult.getErrmsg();
        }
        BaiduCodeEnum baiduCodeEnum = get(baiduBaseResult.getErrno());
        return baiduCodeEnum != null ? baiduCodeEnum.getDesc() : "未知错误, 错误码：" + baiduBaseResult.getErrno();
    }

    public static BaiduCodeEnum get(Integer code) {
        if (code == null) {
            return null;
        }
        for (BaiduCodeEnum value : values()) {
            if (Objects.equals(value.code, code)) {
                return value;
            }
        }
        return null;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getDesc() {
        return desc;
    }
}
