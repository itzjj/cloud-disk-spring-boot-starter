package com.zengjianjun.cloud.disk.platform.baidu.result;

import com.alibaba.fastjson.annotation.JSONField;
import com.zengjianjun.cloud.disk.platform.baidu.enums.BaiduCodeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * @author zengjianjun
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class BaiduBaseResult implements Serializable {

    /**
     * 表示具体错误码。
     * {@link BaiduCodeEnum}
     */
    private Integer errno;

    /**
     * 有关该错误的描述。
     */
    private String errmsg;

    /**
     * 发起请求的请求 Id。
     */
    @JSONField(name = "request_id")
    private String requestId;

    @JSONField(name = "guid_info")
    private String guidInfo;

    private String guid;
}
