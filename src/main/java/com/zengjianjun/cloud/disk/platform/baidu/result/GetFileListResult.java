package com.zengjianjun.cloud.disk.platform.baidu.result;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * 文件列表信息
 *
 * @author zengjianjun
 */
@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class GetFileListResult extends BaiduBaseResult {

    /**
     * 文件数据列表
     */
    private List<FileListInfo> list;

    @Data
    @SuperBuilder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class FileListInfo {

        /**
         * 文件在云端的唯一标识ID
         */
        @JSONField(name = "fs_id")
        private Long fsId;
        /**
         * 文件的绝对路径
         */
        private String path;
        /**
         * 文件名称
         */
        @JSONField(name = "server_filename")
        private String serverFilename;
        /**
         * 文件大小，单位B
         */
        private Integer size;
        /**
         * 文件在服务器修改时间
         */
        @JSONField(name = "server_mtime")
        private Integer serverMtime;
        /**
         * 文件在服务器创建时间
         */
        @JSONField(name = "server_ctime")
        private Integer serverCtime;
        /**
         * 文件在客户端修改时间
         */
        @JSONField(name = "local_mtime")
        private Integer localMtime;
        /**
         * local_ctime
         */
        @JSONField(name = "local_mtime")
        private Integer localCtime;
        /**
         * 是否为目录，0 文件、1 目录
         */
        private Integer isdir;
        /**
         * 文件类型，1 视频、2 音频、3 图片、4 文档、5 应用、6 其他、7 种子
         */
        private Integer category;
        /**
         * 云端哈希（非文件真实MD5），只有是文件类型时，该字段才存在
         */
        private String md5;
        /**
         * 该目录是否存在子目录，只有请求参数web=1且该条目为目录时，该字段才存在， 0为存在， 1为不存在
         */
        @JSONField(name = "dir_empty")
        private Integer dirEmpty;
    }
}
