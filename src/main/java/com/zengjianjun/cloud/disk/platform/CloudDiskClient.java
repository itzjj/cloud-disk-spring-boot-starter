package com.zengjianjun.cloud.disk.platform;

import com.zengjianjun.cloud.disk.enums.CloudDiskTypeEnum;
import com.zengjianjun.cloud.disk.exception.CloudDiskException;
import com.zengjianjun.cloud.disk.platform.baidu.BaiduCloudDiskClient;
import com.zengjianjun.cloud.disk.properties.CloudDiskProperties;
import com.zengjianjun.cloud.disk.result.AuthInfoResult;
import com.zengjianjun.cloud.disk.result.FileInfoResult;
import com.zengjianjun.cloud.disk.result.FileListResult;
import com.zengjianjun.cloud.disk.result.TokenInfoResult;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;

/**
 * @author zengjianjun
 */
@Slf4j
public class CloudDiskClient implements ICloudDiskClient {

    @Resource
    private CloudDiskProperties cloudDiskProperties;
    @Resource
    private BaiduCloudDiskClient baiduCloudDiskClient;

    private ICloudDiskClient client;

    @PostConstruct
    public void init() {
        log.info("cloud disk init start...");
        CloudDiskTypeEnum type = cloudDiskProperties.getType();
        if (type == null) {
            throw CloudDiskException.build("cloud disk init error: type is null");
        }
        if (type == CloudDiskTypeEnum.baidu) {
            client = baiduCloudDiskClient;
        } else {
            throw CloudDiskException.build("cloud disk init error: unsupported cloud disk type");
        }
        log.info("cloud disk load {}", type.name());
        log.info("cloud disk init success...");
    }

    @Override
    public CloudDiskTypeEnum get() {
        return client.get();
    }

    @Override
    public AuthInfoResult getAuthInfo() {
        return client.getAuthInfo();
    }

    @Override
    public void setTokenInfo(TokenInfoResult tokenInfoResult) {
        client.setTokenInfo(tokenInfoResult);
    }

    @Override
    public TokenInfoResult getTokeInfo() {
        return client.getTokeInfo();
    }

    @Override
    public TokenInfoResult refreshAccessToken() {
        return client.refreshAccessToken();
    }

    @Override
    public List<FileListResult> getFileList(String path) {
        return client.getFileList(path);
    }

    @Override
    public FileInfoResult getFileInfo(String fileId) {
        return client.getFileInfo(fileId);
    }

    @Override
    public List<FileInfoResult> getFileInfoList(List<String> fileIdList) {
        return client.getFileInfoList(fileIdList);
    }

    @Override
    public String download(String fileId, String dir) {
        return client.download(fileId, dir);
    }

    @Override
    public String download(String fileId, String dir, Integer size) {
        return client.download(fileId, dir, size);
    }
}
