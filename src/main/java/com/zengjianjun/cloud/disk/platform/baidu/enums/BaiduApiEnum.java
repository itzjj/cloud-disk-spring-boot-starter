package com.zengjianjun.cloud.disk.platform.baidu.enums;

import com.zengjianjun.cloud.disk.enums.IEnum;

/**
 * 百度接口
 *
 * @author zengjianjun
 */
public enum BaiduApiEnum implements IEnum<String, String> {

    GET_AUTH_PAGE("http://openapi.baidu.com/oauth/2.0/authorize?response_type=token&client_id={appKey}&redirect_uri={redirectUri}&scope=basic,netdisk&qrcode=1", "获取百度授权页链接"),
    GET_ACCESS_TOKEN("http://openapi.baidu.com/oauth/2.0/token?grant_type=authorization_code&code={code}&client_id={appKey}&client_secret={secretkey}&redirect_uri={redirectUri}", "获取accessToken"),
    REFRESH_ACCESS_TOKEN("http://openapi.baidu.com/oauth/2.0/token?grant_type=refresh_token&refresh_token={refreshToken}&client_id={appKey}&client_secret={secretkey}", "刷新accessToken"),
    GET_FILE_LIST("http://pan.baidu.com/rest/2.0/xpan/file?method=list&dir={dir}&order=time&start=0&limit=1000&web=1&folder=0&access_token={accessToken}&desc=1", "获取文件列表"),
    GET_FILE_INFO("http://pan.baidu.com/rest/2.0/xpan/multimedia?method=filemetas&access_token={accessToken}&fsids={fsids}&thumb=0&dlink=1&extra=0", "获取文件详情"),
    ;

    BaiduApiEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private final String code;

    private final String desc;

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getDesc() {
        return desc;
    }
}
