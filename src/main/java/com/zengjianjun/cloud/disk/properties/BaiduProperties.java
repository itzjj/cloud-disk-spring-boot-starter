package com.zengjianjun.cloud.disk.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author zengjianjun
 */
@Data
@Component
@ConfigurationProperties(prefix = "cloud.disk.baidu")
public class BaiduProperties {

    private String appId;

    private String appKey;

    private String secretkey;

    private String signKey;

    private String redirectUri;
}
