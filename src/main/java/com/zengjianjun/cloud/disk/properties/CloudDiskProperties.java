package com.zengjianjun.cloud.disk.properties;

import com.zengjianjun.cloud.disk.enums.CloudDiskTypeEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author zengjianjun
 */
@Data
@Component
@ConfigurationProperties(prefix = "cloud.disk")
public class CloudDiskProperties {

    private CloudDiskTypeEnum type;

    private BaiduProperties baiduProperties;
}
