package com.zengjianjun.cloud.disk.config;

import com.zengjianjun.cloud.disk.platform.CloudDiskClient;
import com.zengjianjun.cloud.disk.platform.baidu.BaiduCloudDiskClient;
import com.zengjianjun.cloud.disk.properties.BaiduProperties;
import com.zengjianjun.cloud.disk.properties.CloudDiskProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zengjianjun
 */
@Configuration
@EnableConfigurationProperties({CloudDiskProperties.class, BaiduProperties.class})
public class CloudDiskAutoConfiguration {

    @Bean
    public CloudDiskClient cloudDiskClient() {
        return new CloudDiskClient();
    }

    @Bean
    public BaiduCloudDiskClient baiduCloudDiskClient() {
        return new BaiduCloudDiskClient();
    }
}
