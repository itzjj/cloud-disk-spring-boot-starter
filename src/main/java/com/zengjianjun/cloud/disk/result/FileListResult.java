package com.zengjianjun.cloud.disk.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * 文件列表信息
 *
 * @author zengjianjun
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class FileListResult implements Serializable {

    /**
     * 文件在云端的唯一标识ID
     */
    private String fileId;
    /**
     * 文件的绝对路径
     */
    private String path;
    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 文件大小，单位B
     */
    private Integer size;
    /**
     * 是否为目录，0 文件、1 目录
     */
    private Integer isdir;
    /**
     * 文件类型，1 视频、2 音频、3 图片、4 文档、5 应用、6 其他、7 种子
     */
    private Integer category;
    /**
     * 云端哈希（非文件真实MD5），只有是文件类型时，该字段才存在
     */
    private String md5;
    /**
     * 该目录是否存在子目录，只有请求参数web=1且该条目为目录时，该字段才存在， 0为存在， 1为不存在
     */
    private Integer dirEmpty;
}
