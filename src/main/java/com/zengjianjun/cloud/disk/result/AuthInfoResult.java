package com.zengjianjun.cloud.disk.result;

import com.zengjianjun.cloud.disk.enums.CloudDiskTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 授权信息
 *
 * @author zengjianjun
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class AuthInfoResult implements Serializable {

    /**
     * 所属云平台
     */
    private CloudDiskTypeEnum cloudDiskTypeEnum;
    /**
     * 当前授权信息是否已过期
     * true-已过期, false-未过期
     */
    private boolean authExpired;
    /**
     * 过期时间
     * 当authExpired=false时候该数据有效
     */
    private LocalDateTime expiredTime;
    /**
     * 授权页链接
     */
    private String authPageUrl;
}
