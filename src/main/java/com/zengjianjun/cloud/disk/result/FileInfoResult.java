package com.zengjianjun.cloud.disk.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * 文件详情信息
 *
 * @author zengjianjun
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class FileInfoResult implements Serializable {

    /**
     * 是否是目录，为1表示目录，为0表示非目录
     */
    private Integer isdir;
    /**
     * 文件类型，含义如下：1 视频， 2 音乐，3 图片，4 文档，5 应用，6 其他，7 种子
     */
    private Integer category;
    /**
     * 文件下载链接
     */
    private String dLink;
    /**
     * 文件名
     */
    private String filename;
    /**
     * 文件路径
     */
    private String path;
    /**
     * 文件大小，单位字节
     */
    private Integer size;
    /**
     * 文件md5
     */
    private String md5;
}
