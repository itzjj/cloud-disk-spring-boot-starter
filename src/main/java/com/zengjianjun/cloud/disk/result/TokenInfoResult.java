package com.zengjianjun.cloud.disk.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 认证token信息
 *
 * @author zengjianjun
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class TokenInfoResult implements Serializable {

    /**
     * token信息
     */
    private String token;
    /**
     * 用于刷新token
     */
    private String refreshToken;
    /**
     * 过期时间
     */
    private LocalDateTime expiredTime;
}
